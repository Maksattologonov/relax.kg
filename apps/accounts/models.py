from django.contrib.auth.models import AbstractUser

from django.db import models

from django.utils.translation import ugettext_lazy as _

from apps.accounts.manager import CustomUserManager


class CustomUser(AbstractUser):
    username = None
    email = None
    phone = models.CharField(_('Номер'), max_length=20, unique=True)
    first_name = models.CharField(_('Имя'), max_length=50)
    last_name = models.CharField(_('Фамилия'), max_length=50)
    avatar = models.ImageField(_('Аватар'), upload_to='Media')
    EMAIL_FIELD = None
    USERNAME_FIELD = 'phone'
    REQUIRED_FIELDS = []

    objects = CustomUserManager()