from django.utils.translation import ugettext_lazy as _

from django.contrib.auth import (
    authenticate, login, logout, get_user_model
)
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import PasswordChangeView
from django.core.exceptions import ValidationError

from django.http import (
    HttpResponseRedirect)
from django.shortcuts import render

from django.urls import reverse, reverse_lazy

from django.views import View

from django.views.generic import (
    TemplateView, CreateView, FormView, UpdateView
)
from apps.accounts.forms import (
    LoginForm, UserCreationsForm, PasswordChange, EditProfileForm, EditNumberForm,
    CustomSetPasswordForm
)
from .models import CustomUser

User = get_user_model()


class SignInView(View):

    def post(self, request, *args, **kwargs):

        form = LoginForm(request.POST or None)
        if form.is_valid():
            username = form.cleaned_data['phone']
            password = form.cleaned_data['password']
            user = authenticate(username=username, password=password)
            if user:
                login(request, user)
                return HttpResponseRedirect('edit-profile')
        return render(request, 'pages/sign-in.html', {'form': form})

    def get(self, request):
        form = LoginForm(request.POST or None)
        context = {'form': form}
        return render(request, 'pages/sign-in.html', context)


class RegistrationView(CreateView):
    form_class = UserCreationsForm
    model = CustomUser
    template_name = 'pages/registration.html'
    success_url = 'edit-profile'

    def form_valid(self, form):
        user = form.save(commit=False)
        user.save()
        login(request=self.request, user=user)
        return HttpResponseRedirect(self.success_url)


class RegistrationNumberView(CreateView):
    template_name = 'pages/registration-number.html'
    form_class = EditNumberForm
    success_url = reverse_lazy('registration-submit_page')

    def form_valid(self, form):
        self.request.session['phone_number'] = form.cleaned_data['phone']
        return HttpResponseRedirect(self.get_success_url())


class RegistrationSubmitView(TemplateView):
    template_name = 'pages/registration-submit.html'


class EditProfileView(LoginRequiredMixin, UpdateView):
    model = CustomUser
    form_class = EditProfileForm
    success_url = reverse_lazy('index_page')
    template_name = 'pages/edit-profile.html'

    def get_object(self, queryset=None):
        return self.request.user


class ChangeNumberSubmitView(LoginRequiredMixin, FormView):
    form_class = EditNumberForm
    template_name = 'pages/change-number-submit.html'

    def form_valid(self, form):
        print(form)
        user = self.request.user
        user.phone = form.cleaned_data['phone']
        user.save()
        return HttpResponseRedirect(
            reverse('edit-profile')
        )


class IndexView(TemplateView):
    template_name = 'pages/index.html'


class ChangeNumberView(LoginRequiredMixin, TemplateView):
    template_name = 'pages/change-number.html'


def logout_view(request):
    logout(request)
    return render(request, 'pages/logout.html')


class ForgotPasswordView(TemplateView):
    template_name = 'pages/forgot-password.html'


class ForgotPasswordNumberView(TemplateView):
    template_name = 'pages/forgot-submit.html'


class ChangePasswordView(LoginRequiredMixin, PasswordChangeView):
    form_class = PasswordChange
    template_name = 'pages/change-password.html'
    success_url = reverse_lazy('edit-profile')


class SetNewPasswordView(FormView):
    """View for restoring user password"""
    form_class = CustomSetPasswordForm
    template_name = 'pages/new-password.html'
    success_url = reverse_lazy('sign-in_page')

    def form_valid(self, form):
        number = form.cleaned_data['user']
        try:
            user = User.objects.get(
                phone=number)
        except User.DoesNotExist as err:
            raise ValidationError(
                _('Пользователь с таким номером не найден'), err)
        user.set_password(form.cleaned_data['new_password1'])
        user.save(update_fields=['password'])
        return HttpResponseRedirect(self.get_success_url())
