from django.contrib.auth.forms import PasswordChangeForm

from django import forms

from django.contrib.auth import get_user_model, password_validation

from django.contrib.auth.forms import UserCreationForm

from django.utils.translation import ugettext_lazy as _

from .models import CustomUser


class LoginForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['phone'].label = 'Номер телефона'
        self.fields['password'].label = 'Пароль'

    def clean(self):
        phone = self.cleaned_data['phone']
        password = self.cleaned_data['password']
        if not CustomUser.objects.filter(phone=phone).exists():
            raise forms.ValidationError(f'Пользователь с номером {phone} не найден')
        user = CustomUser.objects.filter(phone=phone).first()
        if user:
            if not user.check_password(password):
                raise forms.ValidationError(f'Неверный пароль')
        return self.cleaned_data

    class Meta:
        model = get_user_model()
        fields = ('phone', 'password')
        widgets = {
            'phone': forms.TextInput(attrs={'class': 'form__input form__input-tel phone_get_number'}),
            'password': forms.PasswordInput(attrs={'placeholder': 'Введите пароль', 'class': 'form__input'})}


class UserCreationsForm(UserCreationForm):
    def __init__(self, *args, **kwargs):
        super(UserCreationForm, self).__init__(*args, **kwargs)
        self.fields['phone'].widget = forms.TextInput(attrs={'class': 'form__input', 'type': 'hidden'})
        self.fields['first_name'].widget = forms.TextInput(
            attrs={'class': 'form__input', 'placeholder': 'Имя'})
        self.fields['last_name'].widget = forms.TextInput(
            attrs={'class': 'form__input', 'placeholder': 'Фамилия'})
        self.fields['password1'].widget = forms.PasswordInput(
            attrs={'class': 'form__input', 'placeholder': 'Придумайте пароль'})
        self.fields['password2'].widget = forms.PasswordInput(
            attrs={'class': 'form__input', 'placeholder': 'Повторите пароль'})

    class Meta:
        model = get_user_model()
        fields = ['phone', 'first_name', 'last_name', 'password1', 'password2']


class PasswordChange(PasswordChangeForm):
    def __init__(self, *args, **kwargs):
        super(PasswordChange, self).__init__(*args, **kwargs)
        self.fields['old_password'].widget = forms.PasswordInput(
            attrs={'class': 'form__input', 'placeholder': 'Введите старый пароль'})
        self.fields['new_password1'].widget = forms.PasswordInput(
            attrs={'class': 'form__input', 'placeholder': 'Придумайте новый пароль'})
        self.fields['new_password2'].widget = forms.PasswordInput(
            attrs={'class': 'form__input', 'placeholder': 'Повторите пароль'})

    class Meta:
        model = CustomUser
        fields = ['old_password', 'new_password1', 'new_password2']


class EditProfileForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(EditProfileForm, self).__init__(*args, **kwargs)
        self.fields['first_name'].widget = forms.TextInput(
            attrs={'class': 'form__input', 'placeholder': 'Введите новое имя'})
        self.fields['last_name'].widget = forms.TextInput(
            attrs={'class': 'form__input', 'placeholder': 'Введите новую фамилию'})

    class Meta:
        model = CustomUser
        fields = ('first_name', 'last_name')


class EditNumberForm(forms.ModelForm):
    class Meta:
        model = get_user_model()
        fields = ('phone',)
        widgets = {
            'phone': forms.TextInput(attrs={})
        }


class CustomSetPasswordForm(forms.Form):

    user = forms.CharField(widget=forms.TextInput(attrs={'type': 'hidden', 'id': 'current-phone'}))

    error_messages = {
        'password_mismatch': _("Пароли не совпадают"),
    }
    new_password1 = forms.CharField(
        widget=forms.PasswordInput(attrs={'class': 'form__input'}),
        strip=False,
        help_text=password_validation.password_validators_help_text_html(),
    )
    new_password2 = forms.CharField(
        strip=False,
        widget=forms.PasswordInput(attrs={'class': 'form__input'}),
    )

    def clean_new_password2(self):
        password1 = self.cleaned_data.get('new_password1')
        password2 = self.cleaned_data.get('new_password2')
        user = self.cleaned_data.get('user')
        if password1 and password2:
            if password1 != password2:
                raise forms.ValidationError(
                    self.error_messages['password_mismatch'],
                    code='password_mismatch',
                )
        password_validation.validate_password(password2)
        return password2


