from django.contrib import admin

# Register your models here.
from apps.taplink.models import (
    ContentBlock, ContentText, ContentMessenger
)


class BlockContentAdmin(admin.ModelAdmin):
    def save_model(self, request, obj, form, change):
        if not obj.created_by:
            obj.created_by = request.user
        obj.save()


admin.site.register(ContentBlock, BlockContentAdmin)


class BlockMessengerAdmin(admin.ModelAdmin):
    pass


admin.site.register(ContentMessenger, BlockMessengerAdmin)


class BlockTextAdmin(admin.ModelAdmin):
    pass


admin.site.register(ContentText, BlockTextAdmin)
