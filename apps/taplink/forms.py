from django import forms
from django.core.exceptions import ValidationError
from django.forms import ModelForm

from .models import ContentBlock


class CreateTaplinkForm(ModelForm):

    class Meta:
        model = ContentBlock
        fields = '__all__'
        widgets = {
            'content_text': forms.Textarea(attrs={'cols': 60, 'rows': 10}),
        }

    # def clean_title(self):
    #     content_text = self.cleaned_data['content_text']
    #     if len(content_text) > 200:
    #         raise ValidationError('Длина превышает 200 символов')
    #
    #     return content_text
