from django.db import models
from django.urls import reverse

from apps.accounts.models import CustomUser
from core import settings
from core.settings import base


class ContentText(models.Model):
    content_text = models.TextField()

    def __str__(self):
        return self.content_text

    class Meta:
        verbose_name = 'Текст'
        verbose_name_plural = 'Текст'


class ContentMessenger(models.Model):
    telegram = models.CharField(max_length=25)
    whatsapp = models.CharField(max_length=25)

    def __str__(self):
        return self.telegram

    class Meta:
        verbose_name = 'Мессенджер'
        verbose_name_plural = 'Мессенджеры'


class ContentBlock(models.Model):
    user = models.ForeignKey(base.AUTH_USER_MODEL, on_delete=models.CASCADE, verbose_name='Пользователь')
    slug = models.SlugField(max_length=255, unique=True, db_index=True, verbose_name='URL')
    avatar = models.ImageField(upload_to='Media', verbose_name='Аватар')
    content_text = models.TextField(verbose_name='Текст')
    telegram = models.CharField(max_length=25, verbose_name='Ссылка на Telegram')
    whatsapp = models.CharField(max_length=25, verbose_name='Ссылка на Whatsapp')
    time_create = models.DateTimeField(auto_now_add=True, verbose_name='Время создания')
    time_update = models.DateTimeField(auto_now=True, verbose_name='Время изменения')
    is_published = models.BooleanField(default=True, verbose_name='Публикация')

    def __str__(self):
        return '{0} {1}'.format(self.user, self.slug)

    class Meta:
        verbose_name = 'Таплинк'
        verbose_name_plural = 'Таплинки'
