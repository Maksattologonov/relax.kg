from django.urls import path

from .views import (
    IndexView, CardView, ShowCardView, CreateTaplinkView, post_create
)

urlpatterns = [
    path('', IndexView.as_view(), name='index_page'),
    path('card/', CardView.as_view(), name='card_page'),
    path('create/', post_create, name='create_page'),
]
