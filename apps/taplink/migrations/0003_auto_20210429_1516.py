# Generated by Django 3.1.7 on 2021-04-29 09:16

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('taplink', '0002_auto_20210429_1513'),
    ]

    operations = [
        migrations.RenameField(
            model_name='contentmessenger',
            old_name='messengers',
            new_name='telegram',
        ),
        migrations.AddField(
            model_name='contentmessenger',
            name='whatsapp',
            field=models.CharField(default=3, max_length=25),
            preserve_default=False,
        ),
    ]
