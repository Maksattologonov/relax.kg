from django.http import Http404
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views import View
from django.views.generic import (
    TemplateView, CreateView, ListView, DetailView, FormView
)

from apps.taplink.forms import CreateTaplinkForm
from apps.taplink.models import ContentBlock


class CardView(ListView):
    model = ContentBlock
    template_name = 'components/Card/Card.html'

    def get_queryset(self):
        return ContentBlock.objects.filter(is_published=True)


class ShowCardView(DetailView):
    model = ContentBlock
    template_name = 'components/Card/detail-card.html'
    slug_url_kwarg = 'post_slug'


class IndexView(TemplateView):
    template_name = 'components/Card/Card.html'


class CreateTaplinkView(View):
    template_name = 'pages/index.html'


def post_create(request):
    if not request.user.is_authenticated:
        raise Http404
    form = CreateTaplinkForm(request.POST or None, request.FILES or None)
    if form.is_valid():
        instance = form.save(commit=False)
        instance.user = request.user
        instance.save()
        print(form)
        return reverse_lazy('card')
    context = {'form': form}
    return render(request, 'pages/index.html', context)

def get(self, request):
    form = self.form
    context = {'form': form}
    return render(request, self.template_name, context)


