document.getElementById("phone-number").innerHTML = sessionStorage.getItem('phone_number_change');


firebase.initializeApp(firebaseConfig);
firebase.analytics();

window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('sign-in-button', {
    'size': 'invisible',
    'callback': (response) => {
        onSignInSubmit();
    }
});
const appVerifier = window.recaptchaVerifier;
document.getElementById('sign-in-button').click()

firebase.auth().signInWithPhoneNumber(sessionStorage.getItem('phone_number_change'), appVerifier)
    .then((confirmationResult) => {
        window.confirmationResult = confirmationResult;
    }).catch((error) => {

    window.recaptchaVerifier.render().then(function (widgetId) {
        grecaptcha.reset(widgetId);
    })
});
document.getElementById('id_phone').value = window.sessionStorage.getItem('phone_number_change')


function sendCode() {
    const code = document.getElementById('form_input_name').value
    confirmationResult.confirm(code)
        .then((result) => {
            $("#form-change-number").submit()
        }).catch((error) => {
        alert('Ошибочно введен код')

    });
}

function addCode() {
    let country_code = document.getElementsByClassName('iti__selected-dial-code')
    let number = document.getElementById('id_phone_number')
    let phoneNumber = document.getElementById('id_phone_number').value = country_code['0'].innerHTML + number.value.replace(/\s+/g, '')
    sessionStorage.setItem('phone_number_change', phoneNumber);
    $("#changeNumberForm").submit()
    window.location.href = 'change-number-submit'
}