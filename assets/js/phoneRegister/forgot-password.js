document.getElementById("phone-number").innerHTML = sessionStorage.getItem('phone_number_for_set_password');

firebase.initializeApp(firebaseConfig);
firebase.analytics();

window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('sign-in-button', {
    'size': 'invisible',
    'callback': (response) => {
        onSignInSubmit();
    }
});

const appVerifier = window.recaptchaVerifier;
document.getElementById('sign-in-button').click()


firebase.auth().signInWithPhoneNumber(sessionStorage.getItem('phone_number_for_set_password'), appVerifier)
    .then((confirmationResult) => {
        window.confirmationResult = confirmationResult;
    }).catch((error) => {

    window.recaptchaVerifier.render().then(function (widgetId) {
        grecaptcha.reset(widgetId);
    })
});

function codeConfirmation() {
    const code = document.getElementById('form_input_id').value
    confirmationResult.confirm(code).then((result) => {
        window.location.href = 'set-password'

    }).catch((error) => {
        alert('код неправильный')
    });
}


function processForm() {
    sessionStorage.clear()
    let country_code = document.getElementsByClassName('iti__selected-dial-code')
    let number = document.getElementById('id_phone_number')
    let phoneNumber = document.getElementById('id_phone_number').value = country_code['0'].innerHTML + number.value.replace(/\s+/g, '')
    sessionStorage.setItem('phone_number_for_set_password', phoneNumber);
    $("#myForm").submit()
    window.location.href = 'forgot-submit'

}