var ui = new firebaseui.auth.AuthUI(firebase.auth());
ui.start('#firebase-auth-container', uiConfig)

firebase.auth().onAuthStateChanged(function (user){
    if (user){
        var displayName = user.displayName

        user.getToken().then(function (accessToken){
            document.getElementById('sign-in-status').textContent = 'Signed in'
            document.getElementById('sign-in').textContent = 'Sign Out'
            document.getElementById('account-details').textContent = JSON.stringify({displayName: displayName},
                null, '  ');
        })
    }else {

    }
})