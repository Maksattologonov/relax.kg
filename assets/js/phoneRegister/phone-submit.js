document.querySelector('#phone-number').innerHTML = window.sessionStorage.getItem('phone_number')


firebase.initializeApp(firebaseConfig);
firebase.analytics();

window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('sign-in-button', {
    'size': 'invisible',
    'callback': (response) => {
        onSignInSubmit();
    }
});

const appVerifier = window.recaptchaVerifier;
document.getElementById('sign-in-button').click()


firebase.auth().signInWithPhoneNumber(sessionStorage.getItem('phone_number'), appVerifier)
    .then((confirmationResult) => {
        window.confirmationResult = confirmationResult;
    }).catch((error) => {

    window.recaptchaVerifier.render().then(function (widgetId) {
        grecaptcha.reset(widgetId);
    })
});

//Проверка кода
function codeConfirmation() {
    const code = document.getElementById('form_input_id').value
    confirmationResult.confirm(code).then((result) => {
        window.location.href = 'registration'

    }).catch((error) => {
        alert('код неправильный')
    });
}


//Добавление кода страны и добавление номера в сессию
function processForm() {
    let country_code = document.getElementsByClassName('iti__selected-dial-code')
    let number = document.getElementById('id_phone_number')
    let phoneNumber = document.getElementById('id_phone_number').value = country_code['0'].innerHTML + number.value.replace(/\s+/g, '')
    window.sessionStorage.setItem('phone_number', phoneNumber);
    window.location.href = 'registration-submit'
}

function addCode() {
    let country_code = document.getElementsByClassName('iti__selected-dial-code')
    let number = document.getElementById('id_phone')
    document.getElementById('id_phone').value = country_code['0'].innerHTML + number.value.replace(/\s+/g, '')
    $("#form-sign-in").submit()
}